/****************************************************************
 * ass1.cpp -  CSCI251/851 Ass1 - Atm2017
 * Function definitions for student database
 * <Jonathan Pham - jp777 - 21/03/2017>
 ****************************************************************/
#include<iomanip> 
#include<cstdlib>
#include<iostream>
#include<fstream>
#include<sstream>
#include<cstring>
#include<string>
using namespace std;

// ============== Constants ==========================================

//Put your consts like: filename, array sizes, etc. here.
const int cMaxRecs = 100;

// ============= User Defined types ==================================

//Put the enum status declaration here
enum StatusType
{
	FT,
	PT
};

// Subject struct to store the subj name and the result the student achieved.
struct Subject
{
	char code[8];
	int result;
};


// StudentRecord struct used to store members/values. 
struct StudentRecord
{
// declare struct menbers here
	unsigned long studentNo;
	char surname[21];
	char firstName[21];
	StatusType mStatus;
	Subject eSubj[4]; // Enrolled subject
};


// ============= Global Variables ====================================

StudentRecord gRecs[cMaxRecs];  // Student Record DB
int gNumRecs = 0;               // Number of records in the student DB

// ============= Private Function Prototypes =========================

int GetStatus(string status);
string PrintStatus(int recNum);
void PrintRecord(int recNum); // Original method
string PrintGrade(int result);
bool StudentNoCheck(int recNum);
bool NameCheck(int recNum, string query);
bool NameErrorCheck(char* name);
void NameConversion(char* name);
bool StatusCheck(int recNum);
bool SubjCheck(int recnNum, int index);
void SubjConversion(char* code);
void FinaliseAddRecord(int confirm);
void WriteFile(); // Original method
void AddPrint();
int SearchRecords(char* search, int& num); // Original method
bool SearchCheck(char* search);
void SearchArray(); // Original method
void ConvertToChar(unsigned long& studentNo, char* studentNoArray);
bool AdvancedSearch(char* search, char* snumArray);
int Response();

// ============= Public Function Definitions =========================

// Reads data file into array
void ReadFile()
{
	ifstream fin; // Declaring input filestream object
	fin.open("students.txt"); // Opens the student database

	if(!fin.good()) // If there is an error reading the file
	{
		cerr << "student.txt, was not found" << endl;
		exit(1);
	}

	gNumRecs = 0; // Resetting  global variable to 0 each time it is called
	fin >> gRecs[gNumRecs].studentNo; 

	while (!fin.eof() &&  gNumRecs < cMaxRecs)
	{
		fin >> gRecs[gNumRecs].surname;
		fin >> gRecs[gNumRecs].firstName;
		string status;
		fin >> status;

		gRecs[gNumRecs].mStatus = StatusType(GetStatus(status));
		int noSubj = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
		for (int i = 0; i < noSubj; i++)
		{
			fin >> gRecs[gNumRecs].eSubj[i].code;
			fin >> gRecs[gNumRecs].eSubj[i].result;
		}

		++gNumRecs;
		fin >> gRecs[gNumRecs].studentNo; // For the while loop to know it has 
										  // reached the end it must first fail
	}

	fin.close(); // Close the file input stream
	cout << "There are " << gNumRecs << " records in the student record";
	cout << " database"  << endl;
}

// Adds new record to the database
void AddRecord()
{
	int confirm;		// Boolean for later confirmation query
	bool query = true;	// While loop query
	while (query)
	{
		// All checks for input paramaters
		while (!StudentNoCheck(gNumRecs));
		while (!NameCheck(gNumRecs, "lname"));
		while (!NameCheck(gNumRecs, "fname"));
		while (!StatusCheck(gNumRecs));
		
		int noSubj = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
		cout << "Enter " << noSubj << " Subjects and the Results: " << endl;
		for (int i = 0; i < noSubj; i++)
		{
			while (!SubjCheck(gNumRecs, i));
		}

		AddPrint();
		cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
		cout << setw(34) << right  << "Confirming input values\n";
		cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;
		confirm = Response();
		
		if(confirm >= 1) // Break the loop for appropriate confirmation
		{
			query = false;
		}
		cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	}

	FinaliseAddRecord(confirm);

}

// Finds and displays specified record on screen
void SearchArray()
{
	char search[9]; // We want only digits so use long to accept only digits.
	int count = -1; // Keep track of what recNum we have searched up to.
	char input = ' '; // input field for later query.

	while (!SearchCheck(search));	// Will perform a check on the search input.

	// The below while loop will iterate through the student records until 
	// completion or the user inputs 'n'
	while (!(count >= gNumRecs - 1 || input == 'n')) 
	{
		input = ' ';	// Reset input value.
		if(SearchRecords(search, count) != -1)
		{
			PrintRecord(count);
			// ---- Print ---- */ cout << "The count is now: " << count << endl;
			// If there are more records it will ask for input.
			while (!(input == 'y' || input == 'n' || count >= gNumRecs - 1)) 
			{
				cout << "Display next record (y/n): ";
				cin >> input;
				cout << endl;
			}
		}
		else
		{
			cout << "Record not found" << endl;
		}
	}
}

// Display all books on screen one at a time
void DisplayRecords()
{
	for (int recNum = 0; recNum < gNumRecs; recNum)
	{
		char response;	// For user input later.
		bool query = true;
		PrintRecord(recNum);

		while (query)
		{
			cout << "Display next record (y/n): ";
			cin >> response;
			cin.ignore(100, '\n');
			cout << endl;

			if(response == 'n' || response == 'y') // Condition to break while.
			{
				query = false;
			}
			else
			{ 
				cout << "Please enter only 'y' or 'n'" << endl;
			}
		}

		if(response == 'n') // Condition to break the for loop.
		{
			recNum = gNumRecs;
		}
		recNum ++;
	}
}

// ============= Private Function Definitions =========================

// -------------- Methods for Enum Status --------------------
// Converts the string input for status into the StatusType enum's index
int GetStatus(string status)
{
	if(status == "FT")
	{
		return 0;
	}
	else if(status == "PT")
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

// Prints out the Status based on what recNum you are looking at 
string PrintStatus(int recNum)
{
	switch(gRecs[recNum].mStatus)
	{
	case FT:
		return "FT";
	case PT:
		return "PT";
	}
}
// -------------------------------------------------------------

// -------------- Methods for Displaying records ---------------
// Prints record i on screen
void PrintRecord(int recNum)
{ 
	cout << "Student No" << setw(5) << " " << gRecs[recNum].studentNo << endl;
	cout << "First Name" << setw(5) << " " << gRecs[recNum].firstName << endl;
	cout << "Family Name" << setw(4) << " " << gRecs[recNum].surname << endl;
	cout << "Status" << setw(9) << " " << PrintStatus(recNum) << endl;

	// Calculation used to work out the subject limit based on status. 
	int subjLimit = (gRecs[recNum].mStatus - 1) * (-2) + 2;
	// cout << "Subjects" << setw(7) << " ";
	cout << setw(15) << left << "Subjects";

	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		cout << gRecs[recNum].eSubj[subjNo].code << " ";
	}

	cout << endl;
	// cout << "Results" << setw(7) << " " << " ";
	cout << setw(16) << left << "Results";

	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		int& mark = gRecs[recNum].eSubj[subjNo].result; 
		cout << setw(3) << left << PrintGrade(mark) << setw(5) << left << mark;
		// cout << PrintGrade(mark) << setw(2) << " " << mark << setw(3) << " ";
	}

	cout << endl;
}

// Converting the numerical mark into a grade for displaying records.
string PrintGrade(int result) 
{
	if(result < 50)
	{
		return "PC";
	}
	else if(result < 65)
	{
		return "P";
	}
	else if(result < 75)
	{
		return "C";
	}
	else if(result < 85)
	{
		return "D";
	}
	else
	{
		return "HD";
	}
}
// -------------------------------------------------------------

// ----------- Error chcking student number input --------------
// Checks to make sure student number is 8 characters long
bool StudentNoCheck(int recNum)
{
	cout << "Student Number: ";
	cin >>  gRecs[recNum].studentNo;
	// /* File input format */ cout << endl;

	if(cin.fail())
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cerr << "Invalid input. Please use digits only" << endl;
		return false;
	}
	else if(gRecs[recNum].studentNo < 10000000 ||
			gRecs[recNum].studentNo > 99999999)
	{
		cin.ignore(1000, '\n'); // Will eat up excess char to return 1 err msg.
		cerr << "Invalid input. Must be 8 digits long" << endl;
		return false;
	}

	cin.ignore(1000, '\n'); // Will eat up excess char to return 1 err msg.
	// ---- Print ---- */ cout << gRecs[recNum].studentNo << endl;
	return true;
}

// ----------- Error student name input -----------------------
// This will run a series of checks on both first and last name 
// and convert it to the appropriate format.
bool NameCheck(int recNum, string query)
{
	if(query == "fname")
	{
		cout << "First Name: ";
	}
	else
	{
		cout << "Family Name: ";

	}

	char name[21];
	cin >> name;
	cin.ignore(1000, '\n');
	// /* File input format */ cout << endl;
	if(!NameErrorCheck(name)){return false;}
	NameConversion(name);

	if(query == "fname")
	{
		strcpy (gRecs[recNum].firstName, name);
		// ---- Print ---- */ cout << gRecs[recNum].firstName << endl;
	}
	else
	{
		strcpy (gRecs[recNum].surname, name);
		// ---- Print ---- */ cout << gRecs[recNum].surname << endl;
	}

	return true;

}

// Checks the name input. Can be used for both first and last name.
bool NameErrorCheck(char* name)
{
	// Checks to see if the name is appropriate length.
	if(strlen(name) > 21)
	{
		cin.ignore(1000, '\n'); // Will eat up excess char to return 1 err msg.
		cerr << "Invalid input. Exceeded 20 char limit" << endl;
		return false;
	}

	// Checks to make sure there are no digits in the name.
	for (int i = 0; i < strlen(name); i++)
	{
		if(isdigit(name[i]))
		{
			cerr << "Invalid input. Contains numbers" << endl;
			return false;
		}
		else if(isspace(name[i]))
		{
			cerr << "Invalid input. Contains white spaces" << endl;
			return false;
		}
	}

	return true;
}


// Name conversion to appropriate form
void NameConversion(char* name)
{
	if(islower(name[0]))
	{
		char u = (name[0] - 32);
		name[0] = u;
		// cout << int(u) << " " << u << " first letter." << endl;
	}

	for (int i = 1; i < strlen(name); i++)
	{
		if(isupper(name[i]))
		{
			char c = (name[i] + 32);
			name[i] = c;
			// cout << c << " at index " << i << endl;
		}
	}
}
// -------------------------------------------------------------

// ----------- -------Error status input -----------------------
// Checks the status of the input 
bool StatusCheck(int recNum)
{
	cout << "Status (FT/PT): ";
	string status; // Create a local status variable because we want 
				   // to confirm the input.
	cin >> status;
	cin.ignore(1000,'\n');
	// /* File input format */ cout << endl;
	// cout << "Status: " << status << endl;

	if(GetStatus(status) == -1)
	{
		cout << "Invalid input. Please enter (FT/PT)." << endl;
		return false;
	}

	gRecs[recNum].mStatus = StatusType(GetStatus(status));
	// ---- Print ----- */ cout << gRecs[recNum].mStatus << endl;
	return true;
}
// -------------------------------------------------------------

// ----------- Error checks subject code & mark input ----------
// Check to see if the subject code and subject result is correct.
bool SubjCheck(int recNum, int index)
{
	char subj[100]; // Temporary variable for subject input.
	int mark;		// Temporary variable for mark input.
	cin >> subj; // Can only enter in two fields for the input.

	// Checks to see if the subject code is 7 char long.
	if(strlen(subj) != 7)
	{
		cin.ignore(1000,'\n');
		cout << "Invalid input. Subject is not 7 char long" << endl;
		cout << "Please try again" << endl;
		return false;
	}

	// Ensures that the format for the subject code is correct.
	if(!isalpha(subj[0]) || !isalpha(subj[1]) || !isalpha(subj[2]) || !isalpha(subj[3])
		|| !isdigit(subj[4]) || !isdigit(subj[5]) || !isdigit(subj[6]))
	{
		cout << "Invalid input. Subject code format is wrong" << endl;
		cout << "Please try again" << endl;
		return false;
	}

	cin >> mark;

	// Checks to see if the mark is between 0 - 100.
	if(cin.fail())
	{
		cin.clear();
		cin.ignore(100, '\n');
		cerr << "Invalid input. Digits only." << endl;
		return false;
	}
	// Checks to see if the mark is between 0 - 100.
	else if(mark < 0 || mark > 100)
	{
		cout << "Invalid input. Mark must be a positive and below 100" << endl;
		cout << "Please try again" << endl;
		return false;

	}
	// If the input satisfies all the checks above it is converted to uppercase
	// and added to the student records
	SubjConversion(subj);
	strcpy(gRecs[recNum].eSubj[index].code, subj);
	gRecs[recNum].eSubj[index].result = mark;
	// ---- Print ---- */cout << "Subject: " << gRecs[recNum].eSubj[index].code;
	// ---- Print ---- */cout << " Result: " << gRecs[recNum].eSubj[index].result << endl;
	return true;
}

// Used to convert Subject Code to all uppercase.
void SubjConversion(char* subject)
{
	for (int i = 0; i < 8; i++)
	{
		if(isalpha(subject[i]) && islower(subject[i]))
		{
			char c = (subject[i] - 32);
			subject[i] = c;
		}
	}
}
// -------------------------------------------------------------

// ----------- Methods used with AddRecord() -------------------
// Method to confirm final input. Returns 1 for accept final input, 0 to decline
// and -1 for an invalid input.
int Response()
{
	int confirm = -1;
	do
	{
		cout << "Do you confirm your input (y/n): "; 
		char response;
		cin >> response;
		cin.ignore(100, '\n');
		if(cin.fail())
		{
			cin.clear();
			cin.ignore(100, '\n');
			cerr << "Invalid input. Enter (y/n)" << endl;
		}
		else if(response == 'y')
		{
			confirm = 1;
		}
		else if(response == 'n')
		{
			confirm = 0;
		}
		// This is a quick hack to cater for the file input.
		else if(response == 's')
		{
			confirm = 2;
		}
	} while (confirm == -1);

	return confirm ;
}

// Prints the added recorded in the requried format.
void AddPrint()
{

	cout << "Student Number: " << gRecs[gNumRecs].studentNo << endl;
	cout << "Family Name: " << gRecs[gNumRecs].surname << endl;
	cout << "First Name: " << gRecs[gNumRecs].firstName << endl;
	cout << "Status (FT/PT) : " << PrintStatus(gNumRecs) << endl;

	int subjLimit = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
	cout << "Enter " << subjLimit << " Subjects and the Results: " << endl;
	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		cout << gRecs[gNumRecs].eSubj[subjNo].code << " ";
		cout << gRecs[gNumRecs].eSubj[subjNo].result << endl;
	}
}

// Writes array to text data file
void WriteFile()
{
	ofstream fout;
	// -- REMEMBER -- At the moment outputting to this text file so i don't 
	//				 have to rewrite the other one all the time.
	fout.open("jono.txt");	

	if(!fout.good())
	{
		cout << "Sorry, can't open or create file" << endl;
		exit(1);
	}

	for(int recNum = 0; recNum < gNumRecs + 1; recNum++)
	{
		fout << gRecs[recNum].studentNo << endl;
		fout << gRecs[recNum].surname << endl;
		fout << gRecs[recNum].firstName << endl;
		fout << PrintStatus(recNum) << endl;

		int subjLimit = (gRecs[recNum].mStatus - 1) * (-2) + 2;
		for (int subjNo = 0; subjNo < subjLimit; subjNo++)
		{
			fout << gRecs[recNum].eSubj[subjNo].code << " ";
			fout << gRecs[recNum].eSubj[subjNo].result << endl;
		}
	}

	fout.close();
}

// Created this method because AddRecord() was too long.
// This method focues at writing to the file and consists of a little hack 
// to cater for the file input so that it will transition to the search.
void FinaliseAddRecord(int confirm)
{
	WriteFile();
	cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout << setw(34) << "Add Record to Database\n";
	cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	AddPrint();
	++gNumRecs;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout << setw(10) << gNumRecs << " records are in the database\n";
	cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;

	if(confirm == 2)
	{
		SearchArray();
	}
}

// -------------------------------------------------------------

// ----------- Methods used with SearchArray() -----------------
// Returns index of matching record or -1 if not found
int SearchRecords(char* search, int& count)
{
	// ---- Print ---- */ cout << "Beginning count: " << count << endl;
	char* snumArray = new char(9);	// Was testing dynamic arrays.
	snumArray[8] = 0;

	for (++count; count < gNumRecs; count++)
	{
		unsigned long& snum = gRecs[count].studentNo;
		ConvertToChar(snum, snumArray);
		// ---- Print ---- */ cout << count << ": " << snumArray << " | " << search << endl;
		// if(strcmp(snumArray, search) == 0)//snumArray == search)
		if(AdvancedSearch(search, snumArray))
		{
			return count;
		}
	}

	count = gNumRecs;
	delete[] snumArray;	// removing from heap
	return -1;	// A -1 will indicate that the search is complete
}


// Checks the search input to make sure it doesnt have 
// alphabetical charaters or spaces.
bool SearchCheck(char* search)
{
	cout << "Enter student number: ";
	string temp;	// Temp variable to check if the input is right format
	// getline(cin,temp,'\n');
	cin >> temp;
	cout << temp << endl;
	/*
	for(int i = 0; i < temp.size()-1; i++)
	{
		 cout << i << "': " << temp[i] << endl;
		 
	}
	// Need this because file input introduces a new line byte for some reason
	// Could be because of linux.
	if(temp[0] == '\n')
	{
		for(int i = 0; i < temp.size()-1; i++)
		{
			temp[i] = temp[i+1];
			cout << "Inside here '" << i << "': " << temp[i] << endl;
			 
		}

		temp = temp.substr(0, temp.size()-1); 
	}
	for(int i = 0; i < temp.size()-1; i++)
	{
		 cout << i << "'- after: " << temp[i] << endl;
		 
	}
	*/
	// Input must be 8 characters long.
	if(!(temp.size() == 8))
	{
		cerr << "Please enter only 8 digits" << endl;
		return false;
	}
	else
	{
		for (int i = 0; i < temp.size(); i++)
		{
			if(isalpha(temp[i]) || temp[i] == ' ' || (ispunct(temp[i]) && temp[i] != '?'))
			{
				cerr << "Invalid input. No alphabet characters or spaces." << endl;
				return false;
			} 

			search[i] = temp[i];
			// cout << "Search: " << search[i] << " Temp: " << temp[i] << endl;
		}
	}

	search[8] = 0;
	return true;
}

// Converts the unsigned long to c-string
void ConvertToChar(unsigned long& studentNo, char* studentNoArray)
{
	stringstream ss;
    ss << studentNo;
    string str = ss.str();
    for (int i = 0; i < str.size(); i++)
    {
        studentNoArray[i] = str[i];
    }   
}

// Takes into consideration of the '?' wildcard
bool AdvancedSearch(char* search, char* snumArray)
{
	for (int i = 0; i < strlen(search); i++)
	{
		if(search[i] != snumArray[i] && search[i] != '?')
		{
			return false;
		}
	}

	return true;
}
