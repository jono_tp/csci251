/****************************************************************
 * ass1.cpp -  CSCI251/851 Ass1 - Atm2017
 * Function definitions for student database
 * <Name - Login - date>
 ****************************************************************/
#include <iomanip> 
#include <cstdlib>
#include<iostream>
#include<fstream>
#include<cstring>
#include<string>
using namespace std;

// ============== Constants ==========================================

//Put your consts like: filename, array sizes, etc. here.
const int cMaxRecs = 100;
const int maxSubj = 4;

// ============= User Defined types ==================================

//Put the enum status declaration here
enum StatusType
{
	FT,
	PT
};

// Subject struct to store the subj name and the result the student achieved.
struct Subject
{
	char code[8];
	int result;
};


// StudentRecord struct used to store members/values. 
struct StudentRecord
{
// declare struct menbers here
	unsigned long studentNo;
	char surname[21];
	char firstName[21];
	StatusType mStatus;
	Subject eSubj[maxSubj]; // Enrolled subject
};


// ============= Global Variables ====================================

StudentRecord gRecs[cMaxRecs];  // Student Record DB
int gNumRecs = 0;               // Number of records in the student DB

// ============= Private Function Prototypes =========================

void PrintRecord(int recNum);
string printGrade(int result);
int SearchRecords(unsigned long search);
void WriteFile();
int noSubjEnrolled(StatusType status); 
int getStatus(string status);
string printStatus(int recNum);
bool statusCheck();
bool studentNoCheck();
bool nameErrorCheck(char name[]);
bool nameCheck(string query);
void nameConversion(char name[]);
void subjConversion(char code[]);
bool checkSubj(int index);
void addRecordPrint();

// ============= Public Function Definitions =========================


// Reads data file into array
void ReadFile()
{
	ifstream fin; // declaring input filestream object
	fin.open("jono.txt"); // Opens the student database

	if(!fin.good()) // If there is an error reading the file
	{
		cerr << "student.txt, was not found" << endl;
		exit(1);
	}

	gNumRecs = 0;
	fin >> gRecs[gNumRecs].studentNo;

	while (!fin.eof() &&  gNumRecs < cMaxRecs)
	{
		fin >> gRecs[gNumRecs].surname;
		fin >> gRecs[gNumRecs].firstName;
		string status;
		fin >> status;

		gRecs[gNumRecs].mStatus = StatusType(getStatus(status));
		int noSubj = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
		for (int i = 0; i < noSubj; i++)
		{
			fin >> gRecs[gNumRecs].eSubj[i].code;
			fin >> gRecs[gNumRecs].eSubj[i].result;
		}

		++gNumRecs;
		fin >> gRecs[gNumRecs].studentNo;
	}

	fin.close();
	cout << "Records are in the database. There are currently ";
	cout << gNumRecs << " records." << endl;
}

// Adds new record to the database
void AddRecord()
{
 	while (!studentNoCheck());
 	cin.ignore(1000,'\n');
 	while (!nameCheck("lname"));
 	while (!nameCheck("fname"));
 	while (!statusCheck());
 	
	int noSubj = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
	cout << "Enter " << noSubj << " Subjects and the Results: " << endl;
	for (int i = 0; i < noSubj; i++)
	{
		while (!checkSubj(i));
	}

	++ gNumRecs;
	WriteFile();
	addRecordPrint();
}

// Check to see if the subject code and subject result is correct.
bool checkSubj(int index)
{
	char subj[100]; // Temporary variable for subject input.
	int mark;		// Temporary variable for mark input.
	cin >> subj >> mark; // Can only enter in two fields for the input.

	// Checks to see if the subject code is 7 char long.
	if(strlen(subj) != 7)
	{
		cout << "Invalid input. Subject is not 7 char long" << endl;
		cout << "Please try again" << endl;
		return false;
	}

	// Ensures that the format for the subject code is correct.
	if(isdigit(subj[0]) || isdigit(subj[1]) || isdigit(subj[2]) || isdigit(subj[3])
		|| isalpha(subj[4]) || isalpha(subj[5]) || isalpha(subj[6]))
	{
		cout << "Invalid input. Subject code format is wrong" << endl;
		cout << "Please try again" << endl;
		return false;
	}

	// Checks to see if the mark is between 0 - 100.
	if(mark < 0 || mark > 100)
	{
		cout << "Invalid input. Mark must be a positive and below 100" << endl;
		cout << "Please try again" << endl;
		return false;

	}
	// If the input satisfies all the checks above it is converted to uppercase
	// and added to the student records
	subjConversion(subj);
	strcpy(gRecs[gNumRecs].eSubj[index].code, subj);
	gRecs[gNumRecs].eSubj[index].result = mark;
	cout << "Subject: " << gRecs[gNumRecs].eSubj[index].code;
	cout << " Result: " << gRecs[gNumRecs].eSubj[index].result << endl;
	return true;
}


// Finds and displays specified record on screen
void SearchArray()
{
	cout << "SearchArray() called." <<endl; // for testing, remove this line
	cout << "Enter student number: ";
	unsigned long search;
	cin >> search;

	if(cin.fail())
	{
		cin.clear();
		cin.ignore(100, '\n');
		cerr << "Invalid input. Enter 8 digits only." << endl;
	}
	else if(SearchRecords(search) != -1)
	{
		PrintRecord(SearchRecords(search));		
	}
	else
	{
		cout << "Record not found" << endl;
	}
}

// Display all books on screen one at a time
void DisplayRecords()
{
	for (int recNum = 0; recNum < gNumRecs; recNum)
	{
		char response;
		bool query = true;
		PrintRecord(recNum);

		while (query)
		{
			cout << "Display next record (y/n): ";
			cin >> response;
			cout << endl;

			if(response == 'n' || response == 'y')
			{
				query = false;
			}
		}

		if(response == 'n'){break;}
		recNum ++;
	}
}



// ============= Private Function Definitions =========================

// Prints record i on screen
void PrintRecord(int recNum)
{ 
	cout << "Student No" << setw(5) << " " << gRecs[recNum].studentNo << endl;
	cout << "First Name" << setw(5) << " " << gRecs[recNum].firstName << endl;
	cout << "Family Name" << setw(4) << " " << gRecs[recNum].surname << endl;
	cout << "Status" << setw(9) << " " << printStatus(recNum) << endl;
	int subjLimit = (gRecs[recNum].mStatus - 1) * (-2) + 2;
	cout << "Subjects" << setw(7) << " ";

	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		cout << gRecs[recNum].eSubj[subjNo].code << " ";
	}

	cout << endl;
	cout << "Results" << setw(7) << " " << " ";

	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		int& mark = gRecs[recNum].eSubj[subjNo].result; 
		cout << printGrade(mark) << setw(2) << " " << mark << setw(3) << " ";
	}

	cout << endl;
}

// Converting the numerical mark into a grade.
string printGrade(int result) 
{
	if(result < 50)
	{
		return "PC";
	}
	else if(result < 65)
	{
		return "P";
	}
	else if(result < 75)
	{
		return "C";
	}
	else if(result < 85)
	{
		return "D";
	}
	else
	{
		return "HD";
	}
}

// Checks to make sure student number is 8 characters long
bool studentNoCheck()
{
	cout << "Student Number: ";
	// unsigned temp;
	// cin >> temp;
	cin >>  gRecs[gNumRecs].studentNo;

	if(cin.fail())
	{
		cin.clear();
		cin.ignore(1000,'\n');
		cerr << "Invalid input. Please use digits only" << endl;
		return false;
	}
	else if(gRecs[gNumRecs].studentNo < 10000000 ||
			gRecs[gNumRecs].studentNo > 99999999)
	{
		cerr << "Invalud input. Must be 8 digits long" << endl;
		return false;
	}

	cout << gRecs[gNumRecs].studentNo << endl;
	return true;
}

// Checks the name input. Can be used for both first and last name.
bool nameErrorCheck(char name[])
{
	// Checks to see if the name is appropriate length.
	if(strlen(name) > 21)
	{
		cerr << "Invalid input. Exceeded 20 char limit" << endl;
		return false;
	}

	// Checks to make sure there are no digits in the name.
	for (int i = 0; i < strlen(name); i++)
	{
		if(isdigit(name[i]))
		{
			cerr << "Invalid input. Contains numbers" << endl;
			return false;
		}
		else if(isspace(name[i]))
		{
			cerr << "Invalid input. Contains white spaces" << endl;
			return false;
		}
	}

	return true;
}

// This will run a check on both first and last name and convert it
// to the appropriate format.
bool nameCheck(string query)
{
	if(query == "fname")
	{
		cout << "First Name: ";
	}
	else
	{
		cout << "Family Name: ";

	}

	char name[21];
	cin.getline(name,21);
	if(!nameErrorCheck(name)){return false;}
	nameConversion(name);

	if(query == "fname")
	{
		strcpy (gRecs[gNumRecs].firstName, name);
		cout << gRecs[gNumRecs].firstName << endl;
	}
	else
	{
		strcpy (gRecs[gNumRecs].surname, name);
		cout << gRecs[gNumRecs].surname << endl;
	}

	return true;

}

// Name conversion to appropriate form
void nameConversion(char name[])
{
	if(islower(name[0]))
	{
		char u = (name[0] - 32);
		name[0] = u;
		// cout << int(u) << " " << u << " first letter." << endl;
	}

	for (int i = 1; i < strlen(name); i++)
	{
		if(isupper(name[i]))
		{
			char c = (name[i] + 32);
			name[i] = c;
			// cout << c << " at index " << i << endl;
		}
	}
}

// Used to convert Subject Code to all uppercase.
void subjConversion(char subject[])
{
	for (int i = 0; i < 8; i++)
	{
		if(isalpha(subject[i]) && islower(subject[i]))
		{
			char c = (subject[i] - 32);
			subject[i] = c;
		}
	}
}

// Returns index of matching record or -1 if not found
int SearchRecords(unsigned long search)
{
	for (int recNum = 0; recNum < gNumRecs; recNum++)
	{
		if(gRecs[recNum].studentNo == search)
		{
			return recNum;
		}
	}

	return -1;
}

// Writes array to text data file
void WriteFile()
{
	ofstream fout;
	fout.open("jono.txt");

	if(!fout.good())
	{
		cout << "Sorry, can't open or create file" << endl;
		exit(1);
	}

	for(int recNum = 0; recNum < gNumRecs; recNum++)
	{
		fout << gRecs[recNum].studentNo << endl;
		fout << gRecs[recNum].surname << endl;
		fout << gRecs[recNum].firstName << endl;
		fout << printStatus(recNum) << endl;

		int subjLimit = (gRecs[recNum].mStatus - 1) * (-2) + 2;
		for (int subjNo = 0; subjNo < subjLimit; subjNo++)
		{
			fout << gRecs[recNum].eSubj[subjNo].code << " ";
			fout << gRecs[recNum].eSubj[subjNo].result << endl;
		}
	}

	fout.close();
}

// Prints the added recorded in the requried format.
void addRecordPrint()
{
	cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout << setw(34) << "Add Record to Database\n";
	cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << "Student Number: " << gRecs[gNumRecs -1].studentNo << endl;
	cout << "Family Name: " << gRecs[gNumRecs -1].surname << endl;
	cout << "First Name: " << gRecs[gNumRecs -1].firstName << endl;
	cout << "Status (FT/PT) : " << printStatus(gNumRecs -1) << endl;

	int subjLimit = (gRecs[gNumRecs -1].mStatus - 1) * (-2) + 2;
	cout << "Enter " << subjLimit << " Subjects and the Results: " << endl;
	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		cout << gRecs[gNumRecs -1].eSubj[subjNo].code << " ";
		cout << gRecs[gNumRecs -1].eSubj[subjNo].result << endl;
	}

	cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout << setw(10) << gNumRecs << " records are in the database\n";
	cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;
}

// ============== Methods for Enum Status ==============================
// Return the number of enrolled subjects based on status
int noSubjEnrolled(StatusType status) 
{
	switch(status)
	{
	case PT:
		return 2;
	case FT:
		return 4;
	}
}

// Converts the file string input for status into the StatusType enum's index
int getStatus(string status)
{
	if(status == "FT") 
	{
		return 0;
	}
	else if(status == "PT")
	{
		return 1;
	}
	else
	{
		cout << "Invalid Status" << endl;
		return -1;
	}
}

// Prints out the Status based on what recNum you are looking at 
string printStatus(int recNum)
{
	switch(gRecs[recNum].mStatus)
	{
	case FT:
		return "FT";
	case PT:
		return "PT";
	}
}

// Checks the status of the input 
bool statusCheck()
{
	cout << "Status (FT/PT): ";
	string input;
	cin >> input;

	if(getStatus(input) == -1)
	{
		return false;
	}

	gRecs[gNumRecs].mStatus = StatusType(getStatus(input));
	cout << gRecs[gNumRecs].mStatus << endl;
	return true;
}

