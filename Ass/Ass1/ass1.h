#ifndef ASS1_H
#define ASS1_H
/***************************************************
*    ass1.h - CSCI251/851 Ass1 - Atm2017           *
*    Header file for student database              *
*    <Jonathan Pham, jp777, 21/03/2017>            *
****************************************************/

void ReadFile();
void DisplayRecords();
void AddRecord();
void SearchArray();

#endif

