/****************************************************************
 * ass1.cpp -  CSCI251/851 Ass1 - Atm201
 * Function definitions for student database
 * <Jonathan Pham - jp777 - 21/03/2017>
 ****************************************************************/
#include<iomanip> 
#include<cstdlib>
#include<iostream>
#include<fstream>
#include<sstream>
#include<cstring>
#include<string>
using namespace std;

// ============== Constants ==========================================

//Put your consts like: filename, array sizes, etc. here.
const int cMaxRecs = 100;

// ============= User Defined types ==================================

//Put the enum status declaration here
enum StatusType
{
	FT,
	PT
};

// Subject struct to store the subj name and the result the student achieved.
struct Subject
{
	char code[8];
	int result;
};


// StudentRecord struct used to store members/values. 
struct StudentRecord
{
// declare struct menbers here
	unsigned long studentNo;
	char surname[21];
	char firstName[21];
	StatusType mStatus;
	Subject eSubj[4]; // Enrolled subject
};


// ============= Global Variables ====================================

StudentRecord gRecs[cMaxRecs];  // Student Record DB
int gNumRecs = 0;               // Number of records in the student DB

// ============= Private Function Prototypes =========================

int GetStatus(string status);
string PrintStatus(int recNum);
void PrintRecord(int recNum); 
string PrintGrade(int result);
bool StudentNoCheck(int recNum);
bool NameCheck(int recNum, string query);
bool NameErrorCheck(char* name);
void NameConversion(char* name);
bool StatusCheck(int recNum);
bool SubjCodeCheck(char* subj);
bool SubjMarkCheck(char* markCheck, int& mark);
bool SubjCheck(int recnNum, int index);
void SubjConversion(char* code);
void WriteFile(); 
bool SearchCheck(char* search);
void ConvertToChar(unsigned long& studentNo, char* studentNoArray);
bool AdvancedSearch(char* search, char* snumArray);
void ErrorPrint(string error, bool ignore);

// ============= Public Function Definitions =========================

// Reads data file into array
void ReadFile()
{
	ifstream fin; // Declaring input filestream object
	fin.open("students.txt"); // Opens the student database

	if(!fin.good()) // If there is an error reading the file
	{
		ErrorPrint("student.txt, was not found", false);
		exit(1);
	}

	gNumRecs = 0; // Resetting  global variable to 0 each time it is called
	fin >> gRecs[gNumRecs].studentNo; 

	while (!fin.eof() &&  gNumRecs < cMaxRecs)
	{
		fin >> gRecs[gNumRecs].surname;
		fin >> gRecs[gNumRecs].firstName;
		string status;
		fin >> status;

		gRecs[gNumRecs].mStatus = StatusType(GetStatus(status));
		int noSubj = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
		for (int i = 0; i < noSubj; i++)
		{
			fin >> gRecs[gNumRecs].eSubj[i].code;
			fin >> gRecs[gNumRecs].eSubj[i].result;
		}

		++gNumRecs;
		fin >> gRecs[gNumRecs].studentNo; // For the while loop to know it has 
										  // reached the end it must first fail
	}

	fin.close(); // Close the file input stream
	cout << "There are " << gNumRecs << " records in the student record";
	cout << " database"  << endl;
}

// Adds new record to the database
void AddRecord()
{
	cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout << setw(34) << right << "Add Record to Database\n";
	cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;

	// All checks for input paramaters
	while (!StudentNoCheck(gNumRecs));
	while (!NameCheck(gNumRecs, "lname"));
	while (!NameCheck(gNumRecs, "fname"));
	while (!StatusCheck(gNumRecs));
	
	int noSubj = (gRecs[gNumRecs].mStatus - 1) * (-2) + 2;
	cout << "Enter " << noSubj << " Subjects and the Results: " << endl;
	for (int i = 0; i < noSubj; i++)
	{
		while (!SubjCheck(gNumRecs, i));
	}

	WriteFile();
	++gNumRecs;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout << setw(10) << right  << gNumRecs << " records are in the database\n";
	cout << "++++++++++++++++++++++++++++++++++++++++++++++" << endl;
}

// Finds and displays specified record on screen
void SearchArray()
{
	char search[9];		// We want only digits so use long to accept only digits
	char input = ' ';	// Input field for later query

	while (!SearchCheck(search));	// Will perform a check on the search input.

	// The below while loop will iterate through the student records until 
	// completion or the user inputs 'n'
	for (int count = 0; count < gNumRecs && input != 'n'; count++)
	{
		char snumArray[9];
		ConvertToChar(gRecs[count].studentNo, snumArray);
		if(AdvancedSearch(search, snumArray))
		{
			PrintRecord(count);
			// If there are more records it will ask for input
			while (!(input == 'y' || input == 'n' || count >= gNumRecs - 1)) 
			{
				cout << "Display next record (y/n): ";
				cin >> input;
				cin.ignore(1000,'\n');	// To remove lots of inputs in the one line
				cout << endl;
			}
		}
		else if (count == gNumRecs - 1)
		{
			cout << "Record not found" << endl;
		}
	}
}

// Display all books on screen one at a time
void DisplayRecords()
{
	for (int recNum = 0; recNum < gNumRecs; recNum)
	{
		char response;	// For user input later
		bool query = true;
		PrintRecord(recNum);

		while (query)
		{
			cout << "Display next record (y/n): ";
			cin >> response;
			cin.ignore(1000, '\n');
			cout << endl;

			if(response == 'n' || response == 'y')	// Condition to break while
			{
				query = false;
			}
			else
			{ 
				cout << "Please enter only 'y' or 'n'" << endl;
			}
		}

		if(response == 'n')	// Condition to break the for loop
		{
			recNum = gNumRecs;
		}
		recNum ++;
	}
}

// ============= Private Function Definitions =========================

// -------------- Methods for Enum Status --------------------
// Converts the string input for status into the StatusType enum's index
int GetStatus(string status)
{
	if(status == "FT")
	{
		return 0;
	}
	else if(status == "PT")
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

// Prints out the Status based on what recNum you are looking at 
string PrintStatus(int recNum)
{
	switch(gRecs[recNum].mStatus)
	{
	case FT:
		return "FT";
	case PT:
		return "PT";
	}
}

// -------------- Methods for Displaying records ---------------
// Prints record i on screen
void PrintRecord(int recNum)
{ 
	cout << setw(15) << left << "Student No" << gRecs[recNum].studentNo << endl;
	cout << setw(15) << left << "First Name" << gRecs[recNum].firstName << endl;
	cout << setw(15) << left << "Family Name" << gRecs[recNum].surname << endl;
	cout << setw(15) << left << "Status" << PrintStatus(recNum) << endl;

	// Calculation used to work out the subject limit based on status
	int subjLimit = (gRecs[recNum].mStatus - 1) * (-2) + 2;
	cout << setw(15) << left << "Subjects";

	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		cout << gRecs[recNum].eSubj[subjNo].code << " ";
	}

	cout << endl;
	cout << setw(16) << left << "Results";

	for (int subjNo = 0; subjNo < subjLimit; subjNo++)
	{
		int& mark = gRecs[recNum].eSubj[subjNo].result; 
		cout << setw(3) << left << PrintGrade(mark) << setw(5) << left << mark;
	}

	cout << endl;
}

// Converting the numerical mark into a grade for displaying records
string PrintGrade(int result) 
{
	if(result < 50)
	{
		return "PC";
	}
	else if(result < 65)
	{
		return "P";
	}
	else if(result < 75)
	{
		return "C";
	}
	else if(result < 85)
	{
		return "D";
	}
	else
	{
		return "HD";
	}
}

// ----------- Error chcking student number input --------------
// Checks to make sure student number is 8 characters long
bool StudentNoCheck(int recNum)
{
	cout << "Student Number: ";
	cin >>  gRecs[recNum].studentNo;

	if(cin.fail())
	{
		ErrorPrint("Please use digits only", true);
		return false;
	}
	else if(gRecs[recNum].studentNo < 10000000 ||
			gRecs[recNum].studentNo > 99999999)
	{
		ErrorPrint("Must be 8 digits long",true);
		return false;
	}

	cin.ignore(1000, '\n');	// Will eat up excess char to return 1 err msg
	return true;
}

// ----------- Error student name input -----------------------
// This will run a series of checks on both first and last name 
// and convert it to the appropriate format
bool NameCheck(int recNum, string query)
{
	if(query == "fname")
	{
		cout << "First Name: ";
	}
	else
	{
		cout << "Family Name: ";

	}

	char name[21];
	cin >> name;
	cin.ignore(1000, '\n');
	if(!NameErrorCheck(name)){return false;}
	NameConversion(name);

	if(query == "fname")
	{
		strcpy(gRecs[recNum].firstName, name);
	}
	else
	{
		strcpy(gRecs[recNum].surname, name);
	}

	return true;

}

// Checks the name input. Can be used for both first and last name
bool NameErrorCheck(char* name)
{
	// Checks to see if the name is appropriate length
	if(strlen(name) > 21)
	{
		ErrorPrint("Exceeded 20 char limit.", true);
		return false;
	}

	// Checks to make sure there are no digits in the name
	for (int i = 0; i < strlen(name); i++)
	{
		if(isdigit(name[i]))
		{
			ErrorPrint("Contains numbers.", false);
			return false;
		}
		else if(isspace(name[i]))
		{
			ErrorPrint("Contains white spaces", false);
			return false;
		}
	}

	return true;
}


// Name conversion to appropriate form
void NameConversion(char* name)
{
	if(!isupper(name[0]))
	{
		char u = (name[0] - 32);
		name[0] = u;
	}

	for (int i = 1; i < strlen(name); i++)
	{
		if(!islower(name[i]))
		{
			char c = (name[i] + 32);
			name[i] = c;
		}
	}
}

// ----------- -------Error status input -----------------------
// Checks the status of the input 
bool StatusCheck(int recNum)
{
	cout << "Status (FT/PT): ";
	string status; // Create a local status variable because we want 
				   // to confirm the input
	cin >> status;
	cin.ignore(1000,'\n');

	if(GetStatus(status) == -1)
	{
		ErrorPrint("Please enter (FT/PT).",false);
		return false;
	}

	gRecs[recNum].mStatus = StatusType(GetStatus(status));
	return true;
}

// ----------- Error checks subject code & mark input ----------
bool SubjCodeCheck(char* subj)
{
	// Checks to see if the subject code is 7 char long
	if(strlen(subj) != 7)
	{
		ErrorPrint("Subject is not 7 char long",true);
		return false;
	}

	// Ensures that the format for the subject code is correct
	if(!isalpha(subj[0]) || !isalpha(subj[1]) || !isalpha(subj[2]) || !isalpha(subj[3])
		|| !isdigit(subj[4]) || !isdigit(subj[5]) || !isdigit(subj[6]))
	{
		ErrorPrint("Subject code format is wrong.",false);
		return false;
	}
	
	return true;
}

bool SubjMarkCheck(char* markCheck, int& mark)
{
	// Originally used to make sure there were no decimals. But weird return
	for (int i = 0; i < 3; i++)
	{
		if((isalpha(markCheck[i]) || ispunct(markCheck[i])))
		{
			ErrorPrint("Enter digits only.",true);
			return false;
		}
	}

	mark = atoi(markCheck);		// Converts cstring to int
	// Checks to see if the mark is between 0 - 100
	if(cin.fail())
	{
		ErrorPrint("",true);
		return false;
	}
	else if(mark < 0 || mark > 100)
	{
		ErrorPrint("Mark must be positive and below 100.",false);
		return false;

	}

	return true;
}

// Check to see if the subject code and subject result is correct
bool SubjCheck(int recNum, int index)
{
	char subj[100]; // Temporary variable for subject input
	cin >> subj; // Can only enter in two fields for the input
	if(!SubjCodeCheck(subj)){return false;}

	char markCheck[4];		// Temporary variable for mark input
	int mark;
	cin >> markCheck;
	if(!SubjMarkCheck(markCheck, mark)){return false;}

	// If the input satisfies all the checks above it is converted to uppercase
	// and added to the student records
	SubjConversion(subj);
	strcpy(gRecs[recNum].eSubj[index].code, subj);
	gRecs[recNum].eSubj[index].result = mark;
	return true;
}

// Used to convert Subject Code to all uppercase
void SubjConversion(char* subject)
{
	for (int i = 0; i < 8; i++)
	{
		if(isalpha(subject[i]) && islower(subject[i]))
		{
			char c = (subject[i] - 32);
			subject[i] = c;
		}
	}
}

// Writes array to text data file
void WriteFile()
{
	ofstream fout;
	fout.open("jono.txt");	

	if(!fout.good())
	{
		cout << "Sorry, can't open or create file" << endl;
		exit(1);
	}

	for(int recNum = 0; recNum < gNumRecs + 1; recNum++)
	{
		fout << gRecs[recNum].studentNo << endl;
		fout << gRecs[recNum].surname << endl;
		fout << gRecs[recNum].firstName << endl;
		fout << PrintStatus(recNum) << endl;

		int subjLimit = (gRecs[recNum].mStatus - 1) * (-2) + 2;
		for (int subjNo = 0; subjNo < subjLimit; subjNo++)
		{
			fout << gRecs[recNum].eSubj[subjNo].code << " ";
			fout << gRecs[recNum].eSubj[subjNo].result << endl;
		}
	}

	fout.close();
}

// Checks the search input to make sure it doesnt have 
// alphabetical charaters or spaces.
bool SearchCheck(char* search)
{
	cout << "Enter student number: ";
	string temp;	// Temp variable to check if the input is right format
	cin >> temp;
	if(!(temp.size() == 8))
	{
		ErrorPrint("Please enter only 8 digits",true);
		return false;
	}
	else
	{
		for (int i = 0; i < temp.size(); i++)
		{
			if(isalpha(temp[i]) || temp[i] == ' ' || (ispunct(temp[i]) && temp[i] != '?'))
			{
				ErrorPrint("No alphabet characters or spaces.",true);
				return false;
			} 

			search[i] = temp[i];
		}
	}

	search[8] = 0;
	return true;
}

// Converts the unsigned long to c-string
void ConvertToChar(unsigned long& studentNo, char* studentNoArray)
{
	stringstream ss;
    ss << studentNo;
    string str = ss.str();
    for (int i = 0; i < str.size(); i++)
    {
        studentNoArray[i] = str[i];
    }   
}

// Takes into consideration of the '?' wildcard
bool AdvancedSearch(char* search, char* snumArray)
{
	for (int i = 0; i < strlen(search); i++)
	{
		if(search[i] != snumArray[i] && search[i] != '?'){return false;}
	}

	return true;
}

// Printing default error message
void ErrorPrint(string error, bool ignore)
{
	cin.clear();
	if(ignore)
	{
		cin.ignore(1000, '\n');	// To remove lots of inputs in the one line
	}
	cerr << "Invalid input. Please try again. " << error << endl;
}
