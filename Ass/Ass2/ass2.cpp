/***********************************************************************************
 * CSCI251/851 - Assignment 2
 * ass2.cpp - Contains function definitions for phone database program
 * Put you name, login and the date last modified here.
 *
 ***********************************************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <string>
#include<sstream>
using namespace std;

// ============== Constants ==========================================

const char cDataFileName[] = "phone1.txt";
const int cMaxRecs = 250;  // Max records in database
const int cMaxNChars = 20; // Max chars in name
const int cMaxLChars = 30; // Max chars in location
const int cMaxPChars = 8;  // Max chars in phone no


// ============ User Defined types ==================================

struct PhoneRecord
{
	char PhoneNo[cMaxPChars+1];
	char FamilyName[cMaxNChars+1];
	char GivenName[cMaxNChars+1];
	int StreetNo;
	char StreetName[cMaxLChars+1];
	char Suburb[cMaxLChars+1];
	int PostCode;
};

// ============= Private Function Prototypes =========================

void DisplayRecord(int recNum);
void WriteFile();
bool NameCheck(int recNum, string query);
bool NameErrorCheck(char* name, int nameLength);
void NameConversion(char* name);
bool NumberCheck(int recNum, int limit);
void ErrorPrint(string error, bool ignore);


// ============= Global Data =========================================

PhoneRecord* gRecs[cMaxRecs];
int gNumRecs=0;


// ============= Private Function Prototypes =========================

void ReadFile()
{//Load Phone records from text datafile into array
	ifstream fin;
	fin.open(cDataFileName);
	if (!fin.good())
	{
		cerr << "Could not open data file!\n";
		exit(1);
	}
	gNumRecs=0;
	int i;
	for(i=0;i<cMaxRecs;i++)
	{
		gRecs[i] = new PhoneRecord;
		fin >> gRecs[i]->PhoneNo;
		if (fin.fail()){break;} // then eof
		fin >> gRecs[i]->FamilyName;
		fin >> gRecs[i]->GivenName;
		fin >> gRecs[i]->StreetNo;
		fin.ignore();
		fin.getline(gRecs[i]->StreetName,cMaxLChars+1);
		fin.getline(gRecs[i]->Suburb,cMaxLChars+1);
		fin >> gRecs[i]->PostCode; 
		// cout << "[" << i << "]:" << gRecs[i]->PhoneNo << endl;
		// cout << "[" << i << "]:" << gRecs[i]->FamilyName << endl;
		// cout << "[" << i << "]:" << gRecs[i]->GivenName << endl;
		// cout << "[" << i << "]:" << gRecs[i]->StreetNo << endl;
		// cout << "[" << i << "]:" << gRecs[i]->StreetName << endl;
		// cout << "[" << i << "]:" << gRecs[i]->Suburb << endl;
		DisplayRecord(i);
	}
	gNumRecs=i;
	fin.close();
	cout<< "\nThere are "<< gNumRecs <<" records in the Phone database\n";
}

void DisplayRecords() // Displays record i on screen
{
	for (int recNum = 0; recNum < gNumRecs; recNum)
	{
		char response;	// For user input later
		bool query = true;
		DisplayRecord(recNum);

		while (query && (recNum % 5 == 0) && recNum != 0)
		{
			cout << "Display more records? (y/n): ";
			cin >> response;
			cin.ignore(1000, '\n');
			cout << endl;

			if (response == 'n' || response == 'y')	// Condition to break while
			{
				query = false;
			}
			else
			{ 
				cout << "Please enter only 'y' or 'n'" << endl;
			}
		}

		if (response == 'n')	// Condition to break the for loop
		{
			recNum = gNumRecs;
		}
		recNum ++;
	}
}

void DisplayRecord(int recNum)
{ 
	cout << setw(12) << left << gRecs[recNum]->PhoneNo << " ";
	cout << gRecs[recNum]->GivenName << " " << gRecs[recNum]->FamilyName;
	cout << ", " << gRecs[recNum]->StreetNo << " " << gRecs[recNum]->StreetName;
	cout << ", " << gRecs[recNum]->Suburb << ", " << gRecs[recNum]->PostCode;
	cout << endl;
}

// ============= Public Function Definitions =========================

void AddRecord()
{// Adds a new record to the database array

	cout<<"AddRecord()- Not yet implemented\n";

	// if DB not full
	if (gNumRecs != cMaxRecs)
	{
		gRecs[gNumRecs] = new PhoneRecord;
		while (!NumberCheck(gNumRecs, 8));
		while (!NameCheck(gNumRecs, "lname"));
		while (!NameCheck(gNumRecs, "fname"));
		while (!NumberCheck(gNumRecs, 6));
		while (!NameCheck(gNumRecs, "stName"));
		while (!NameCheck(gNumRecs, "suburb"));
		while (!NumberCheck(gNumRecs, 4));
		// DisplayRecord(gNumRecs);
		WriteFile();
		++gNumRecs;
		cout << "A new record has been added to the database" << endl;
		cout << "...there are " << gNumRecs << " records in the database" << endl;
	}
	else
	{
		cerr << "Error: DB Full!" << endl;
	}
}

// Writes array to text data file
void WriteFile()
{
	ofstream fout;
	fout.open(cDataFileName,ios::app);

	if (!fout.good())
	{
		cout << "Sorry, can't open or create file" << endl;
		exit(1);
	}

	fout << gRecs[gNumRecs]->PhoneNo << endl;
	fout << gRecs[gNumRecs]->FamilyName << endl;
	fout << gRecs[gNumRecs]->GivenName << endl;
	fout << gRecs[gNumRecs]->StreetNo << endl;
	fout << gRecs[gNumRecs]->StreetName << endl;
	fout << gRecs[gNumRecs]->Suburb << endl;
	fout << gRecs[gNumRecs]->PostCode << endl;

	fout.close();
}
void SearchRecords()
{
	// Searches database array for phone number 

	bool found = false;
	char input[9];
	cout << "Enter phone number: ";
	cin >> input;

	for (int recNum = 0; recNum < gNumRecs && !found; recNum++)
	{
		if (strcmp(input, gRecs[recNum] -> PhoneNo) == 0)
		{
			found = true;
			cout << gRecs[recNum] -> GivenName << ", " << gRecs[recNum] -> FamilyName;
			cout << ", " << gRecs[recNum] -> StreetNo << ", " << gRecs[recNum] -> StreetName;
			cout << ", " << gRecs[recNum] -> Suburb << ", " << gRecs[recNum] -> PostCode;
			cout << endl;
		}
	}
	if(!found)
	{
		cout << "Record not found!" << endl;
	}
}


void CleanUp()
{// Deletes all dynamic data in gRecs array

	for (int recNum = 0; recNum < gNumRecs; recNum++)
	{
		delete [] gRecs[recNum];
	}

	cout<<"\n\t*** Thanks for using the Phone DB ***\n";
}
// ============= Private Functions Definitions =========================

bool NumberCheck(int recNum, int limit)
{
	string number;
	if (limit == 8)
	{
		cout << "Enter your phone Number: ";
	}
	else if (limit == 4)
	{
		cout << "Enter postcode: ";
	}
	else 
	{
		cout << "Enter street number: ";
	}
	cin >> number;
	// cout << "size: " << number.size() << ", limit: " << limit << endl;
	if (number.size() > limit){return false;}

	for (int i = 0; i < limit; i++)
	{
		if (!isdigit(number[i]) && limit != 6)
		{
			// cout << i << ": " << number[i] << endl;
			ostringstream ss;
			ss << limit;
			string convert = ss.str();
			string error = "Enter only " + convert + " digits";
			ErrorPrint(error, false);
			return false;
		}
	}

	if (limit == 8)
	{
		strncpy(gRecs[recNum]->PhoneNo, number.c_str(), limit + 1);
		// cout << "[" << recNum << "]:" << gRecs[recNum]->PhoneNo << endl;
	}
	else if (limit == 4)
	{
		int postcode = atoi(number.c_str());
		gRecs[recNum]->PostCode =  postcode;
		// cout << "[" << recNum << "]:" << gRecs[recNum]->PostCode << endl;
	}
	else 
	{
		int stNo = atoi(number.c_str());
		if (stNo < 1 || stNo > 2500)
		{
			ErrorPrint("Enter a number between 1 and 2500", false);
			return false;
		}
		gRecs[recNum]->StreetNo = stNo;
		// cout << "[" << recNum << "]:" << gRecs[recNum]->StreetNo << endl;
	}

	cin.ignore(1000, '\n');	// Will eat up excess char to return 1 err msg
	return true;
}

bool NameCheck(int recNum, string query)
{
	if (query == "fname")
	{
		cout << "Enter given name: ";
	}
	else if (query ==  "lname")
	{
		cout << "Enter family name: ";
	}
	else if (query == "stName")
	{
		cout << "Enter street name: ";
	}
	else
	{
		cout << "Enter suburb name: ";
	}

	int nameLength = 31;
	if (query == "fname" || query == "lname")
	{
		nameLength = 21;
	}
	char name[nameLength];
	cin.getline(name, nameLength);
	if (!NameErrorCheck(name, nameLength)){return false;}
	NameConversion(name);

	if (query == "fname")
	{
		strcpy(gRecs[recNum]->GivenName, name);
		// cout << "[" << recNum << "]:" << gRecs[recNum]->GivenName << endl;
	}
	else if (query ==  "lname")
	{
		strcpy(gRecs[recNum]->FamilyName,  name);
		// cout << "[" << recNum << "]:" << gRecs[recNum]->FamilyName << endl;
	}
	else if (query == "stName")
	{
		strcpy(gRecs[recNum]->StreetName, name);
		// cout << "[" << recNum << "]:" << gRecs[recNum]->StreetName << endl;
	}
	else
	{
		strcpy(gRecs[recNum]->Suburb, name);
		// cout << "[" << recNum << "]:" << gRecs[recNum]->Suburb << endl;
	}

	return true;
}

bool NameErrorCheck(char* name, int nameLength)
{
	// Checks to see if the name is appropriate length
	if (strlen(name) > nameLength)
	{
		ErrorPrint("Exceeded char limit.", true);
		return false;
	}

	// Checks to make sure there are no digits in the name
	for (int i = 0; i < strlen(name); i++)
	{
		if (isdigit(name[i]))
		{
			ErrorPrint("Contains numbers.", false);
			return false;
		}
	}

	return true;
}

// Name conversion to appropriate form
void NameConversion(char* name)
{
	if (!isupper(name[0]))
	{
		char u = (name[0] - 32);
		name[0] = u;
	}

	for (int i = 1; i < strlen(name); i++)
	{
		if (!islower(name[i]) && name[i] != ' ' && name[i-1] != ' ')
		{
			char c = (name[i] + 32);
			name[i] = c;
		}
		else if (name[i] == ' ' && islower(name[i+1]))
		{
			char p = (name[++i] - 32);
			name[i] = p;
		}
	}
}

// Printing default error message
void ErrorPrint(string error, bool ignore)
{
	cin.clear();
	if (ignore)
	{
		cin.ignore(1000, '\n');	// To remove lots of inputs in the one line
	}
	cerr << "Invalid input. Please try again. " << error << endl;
}

// Linux fix for text files. Converting text file from windows format to linux
// awk '{sub("\r$","");print }' original.txt > new.txt
